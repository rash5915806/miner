package com.example.miner.controller;

import com.example.miner.entity.Field;
import com.example.miner.entity.NewGameRequest;
import com.example.miner.entity.TurnRequest;
import com.example.miner.response.ErrorResponse;
import com.example.miner.response.TurnResponse;
import com.example.miner.response.Response;
import com.example.miner.service.FieldGenerator;
import com.example.miner.service.FieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class GameController {
    private final List<Field> gameFields = new ArrayList<>();
    private final FieldGenerator fieldGenerator;
    private final FieldService fieldService;

    @Autowired
    public GameController(FieldGenerator fieldGenerator, FieldService fieldService) {
        this.fieldGenerator = fieldGenerator;
        this.fieldService = fieldService;
    }

    @PostMapping("/turn")
    public ResponseEntity<Response> turn(@RequestBody() TurnRequest request) {
        Field requestGameField = gameFields.stream()
                .filter(field -> field.getGameUUID().equals(request.getGameId()))
                .findFirst()
                .orElse(null);

        if (requestGameField == null)
            return ResponseEntity.badRequest().body(new ErrorResponse("игра с таким идентификатором не найдена"));

        if (requestGameField.getCompleted())
            return ResponseEntity.badRequest().body(new ErrorResponse("Игра завершена"));

        if (!requestGameField.getGameField()[request.getRow()][request.getCol()].isBlank())
            return ResponseEntity.badRequest().body(new ErrorResponse("Уже открытая ячейка"));

        requestGameField = fieldService.turn(requestGameField, request.getRow(), request.getCol());

        return ResponseEntity.ok(TurnResponse.builder()
                .gameId(requestGameField.getGameUUID())
                .width(requestGameField.getWidth())
                .height(requestGameField.getHeight())
                .minesCount(requestGameField.getMinesCount())
                .completed(requestGameField.getCompleted())
                .field(requestGameField.getGameField())
                .build());
    }

    @PostMapping(value = "/new")
    public ResponseEntity<Response> createNewGame(@RequestBody() NewGameRequest request) {
        if (request.getWidth() > 30 || request.getWidth() < 2 || request.getHeight() > 30 || request.getHeight() < 2) {
            return ResponseEntity.badRequest().body(
                    new ErrorResponse("ширина и высота должны быть не меньше 2 и не больше 30")
            );
        }

        Integer totalFields = request.getWidth() * request.getHeight();
        if (request.getMinesCount() >= totalFields) {
            return ResponseEntity.badRequest().body(
                    new ErrorResponse(
                            String.format("количество мин должно быть не менее 1 и не более %d", totalFields - 1)));
        }

        Field newField = Field.builder()
                .gameUUID(UUID.randomUUID())
                .gameField(fieldGenerator.generateEmptyField(request.getWidth(), request.getHeight()))
                .bombsField(fieldGenerator.generateBombsField(request.getWidth(), request.getHeight(), request.getMinesCount()))
                .width(request.getWidth())
                .height(request.getHeight())
                .minesCount(request.getMinesCount())
                .completed(false)
                .gameStartTime(LocalDateTime.now())
                .build();

        gameFields.add(newField);

        return ResponseEntity.ok(TurnResponse.builder()
                .gameId(newField.getGameUUID())
                .width(request.getWidth())
                .height(request.getHeight())
                .minesCount(request.getMinesCount())
                .completed(false)
                .field(newField.getGameField())
                .build()
        );
    }
}