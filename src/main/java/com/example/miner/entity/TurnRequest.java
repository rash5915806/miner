package com.example.miner.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.UUID;

@Data
public class TurnRequest {
    private UUID gameId;
    private Integer col;
    private Integer row;

    public TurnRequest(@JsonProperty("game_id") UUID gameId,
                       @JsonProperty("col") Integer col,
                       @JsonProperty("row") Integer row) {
        this.gameId = gameId;
        this.col = col;
        this.row = row;
    }
}