package com.example.miner.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class NewGameRequest {
    private Integer width;
    private Integer height;
    private Integer minesCount;

    public NewGameRequest(@JsonProperty("width") Integer width,
                          @JsonProperty("height") Integer height,
                          @JsonProperty("mines_count") Integer minesCount) {
        this.width = width;
        this.height = height;
        this.minesCount = minesCount;
    }
}