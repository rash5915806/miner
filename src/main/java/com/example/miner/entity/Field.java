package com.example.miner.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Builder
public class Field {
    private UUID gameUUID;
    private String[][] gameField;
    private String[][] bombsField;
    private Integer height;
    private Integer width;
    private Integer minesCount;
    private Boolean completed;
    private LocalDateTime gameStartTime;

}