package com.example.miner.service;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class FieldGenerator {
    public String[][] generateEmptyField(int width, int height) {
        String[][] field = new String[height][width];

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                field[i][j] = " ";
            }
        }

        return field;
    }

    public String[][] generateBombsField(int width, int height, int minesCount) {
        String[][] field = new String[height][width];

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                field[i][j] = " ";
            }
        }

        Random rand = new Random();
        for (int i = 0; i < minesCount; i++) {
            int mineX, mineY;
            do {
                mineX = rand.nextInt(width);
                mineY = rand.nextInt(height);
            } while ("X".equals(field[mineY][mineX]));
            field[mineY][mineX] = "X";
        }

        return field;
    }
}