package com.example.miner.service;

import com.example.miner.entity.Field;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class FieldService {
    public Field turn(Field field, int row, int col) {
        if (field.getBombsField()[row][col].equals("X")) {
            for (int i = 0; i < field.getBombsField().length; i++) {
                for (int j = 0; j < field.getBombsField()[0].length; j++) {
                    if (field.getBombsField()[i][j].equals("X")) {
                        field.getGameField()[i][j] = "X";
                    } else {
                        int bombCount = countBombs(field, i, j);
                        field.getGameField()[i][j] = bombCount > 0 ? String.valueOf(bombCount) : "0";
                    }
                }
            }
            field.setCompleted(true);
            return field;
        }

        field.setGameStartTime(LocalDateTime.now());
        openField(field, row, col);

        if (checkWin(field)) {
            field.setCompleted(true);
            revealBombs(field);
        }

        return field;
    }

    private boolean checkWin(Field field) {
        int unopenedCount = 0;
        for (int i = 0; i < field.getGameField().length; i++) {
            for (int j = 0; j < field.getGameField()[0].length; j++) {
                if (field.getGameField()[i][j].equals(" ")) {
                    unopenedCount++;
                }
            }
        }
        return unopenedCount == field.getMinesCount();
    }

    private void revealBombs(Field field) {
        for (int i = 0; i < field.getBombsField().length; i++) {
            for (int j = 0; j < field.getBombsField()[0].length; j++) {
                if (field.getBombsField()[i][j].equals("X")) {
                    field.getGameField()[i][j] = "M";
                }
            }
        }
    }

    private int countBombs(Field field, int row, int col) {
        int bombCount = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                int newRow = row + i;
                int newCol = col + j;
                if (newRow >= 0 && newCol >= 0 && newRow < field.getBombsField().length && newCol < field.getBombsField()[0].length) {
                    if (field.getBombsField()[newRow][newCol].equals("X")) {
                        bombCount++;
                    }
                }
            }
        }
        return bombCount;
    }

    private void openField(Field field, int row, int col) {
        if (row < 0 || col < 0 || row >= field.getBombsField().length || col >= field.getBombsField()[0].length) {
            return;
        }

        if (!field.getGameField()[row][col].equals(" ") || field.getBombsField()[row][col].equals("X")) {
            return;
        }

        int bombCount = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                int newRow = row + i;
                int newCol = col + j;
                if (newRow >= 0 && newCol >= 0 && newRow < field.getBombsField().length && newCol < field.getBombsField()[0].length) {
                    if (field.getBombsField()[newRow][newCol].equals("X")) {
                        bombCount++;
                    }
                }
            }
        }

        if (bombCount > 0) {
            field.getGameField()[row][col] = String.valueOf(bombCount);
        } else {
            field.getGameField()[row][col] = "0";
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    openField(field, row + i, col + j);
                }
            }
        }
    }
}