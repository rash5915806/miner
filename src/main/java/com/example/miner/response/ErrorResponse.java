package com.example.miner.response;

import lombok.*;

@Getter
@RequiredArgsConstructor
public class ErrorResponse extends Response {
    private final String error;
}