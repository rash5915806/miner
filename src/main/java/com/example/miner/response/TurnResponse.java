package com.example.miner.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Builder
@Getter
@Setter
public class TurnResponse extends Response {
    @JsonProperty("game_id")
    private final UUID gameId;
    @JsonProperty("width")
    private final int width;
    @JsonProperty("height")
    private final int height;
    @JsonProperty("mines_count")
    private final int minesCount;
    @JsonProperty("completed")
    private final boolean completed;
    @JsonProperty("field")
    private final String[][] field;
}